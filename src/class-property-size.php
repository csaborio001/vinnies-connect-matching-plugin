<?php 

namespace ScorpioTek;

class Property_Size extends FancyLogger {

	// Properties
	private $options_value;
	private $weight;

	public function __construct( $total_options, $weight ) {
		$this->set_options_value( 1 / $total_options );
		$this->set_weight( $weight );
	}


	/**
	 * Setter for the options_value property.
	 *
	 * @param string $options_value the new value of the options_value property.
	 */
	public function set_options_value( $options_value ) {
		$this->options_value = $options_value;
		return $this->get_options_value();
	}
	/**
	 * Getter for the options_value property.
	 *
	 * @return string - the value of the options_value property.
	 */
	public function get_options_value() {
		return $this->options_value;
	}

	/**
	 * Setter for the weight property.
	 *
	 * @param string $weight the new value of the weight property.
	 */
	public function set_weight( $weight ) {
		$this->weight = $weight;
		return $this->get_weight();
	}
	/**
	 * Getter for the weight property.
	 *
	 * @return string - the value of the weight property.
	 */
	public function get_weight() {
		return $this->weight;
	}

}
