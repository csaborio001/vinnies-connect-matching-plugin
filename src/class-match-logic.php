<?php 

namespace ScorpioTek;

use Compeer\Client;
use Compeer\Volunteer;
use ScorpioTek\Match_Constants;

class Match_Logic extends FancyLogger {
	private $match_total        = 0;
	private $matched_properties = 0;

	private $match_constants = null;

	public function __construct() {
		parent::__construct();
		$this->set_match_constants( new Match_Constants() );
	}

	/**
	 * When two participants are compared, we need to keep track of the valid number of comparisons made.
	 * This functions gets the properties that are used in the matching algorithm and sends them over
	 * to the calculate_property, which makes the appropriate comparison to update the match_total property
	 * of this class.
	 *
	 * @since    0.0.1
	 * @param    int               $client_id             The post ID of the client to compare.
	 * @param    int               $volunteer_id          The post ID of the volunteer to compare.
	 * @return   int               $match_total           The total number of valid comparisons that could be made
	 *                             between the two participants.
	 */
	public function calculate_size( $client_id, $volunteer_id ) {
		$client    = new Client( $client_id );
		$volunteer = new Volunteer( $volunteer_id );

		$client_value    = $client->get_living_situation();
		$volunteer_value = $volunteer->get_living_situation();
		$this->calculate_property( $client_value, $volunteer_value, 'Living Situation', 0 );

		$client_value    = $client->get_marital_status();
		$volunteer_value = $volunteer->get_marital_status();
		$this->calculate_property( $client_value, $volunteer_value, 'Marital Status', 0 );

		$client_value    = $client->get_lgtbi();
		$volunteer_value = $volunteer->get_lgtbi();
		$this->calculate_property( $client_value, $volunteer_value, 'LGTBI', 0 );

		$client_value    = $client->get_atsi();
		$volunteer_value = $volunteer->get_atsi();
		$this->calculate_property( $client_value, $volunteer_value, 'ATSI', 0 );

		$client_value    = $client->get_smoker();
		$volunteer_value = $volunteer->get_smoker();
		$this->calculate_property( $client_value, $volunteer_value, 'Smoker', 0 );

		$client_value    = $client->get_country_of_birth();
		$volunteer_value = $volunteer->get_country_of_birth();
		$this->calculate_property( $client_value, $volunteer_value, 'Country of Birth', 0 );

		$client_value    = $client->get_languages_spoken();
		$volunteer_value = $volunteer->get_languages_spoken();
		$this->calculate_property( $client_value, $volunteer_value, 'Languages Spoken', 1 );

		$client_value    = $client->get_outdoor_activities();
		$volunteer_value = $volunteer->get_outdoor_activities();
		$this->calculate_property( $client_value, $volunteer_value, 'Outdoor Activities', 1 );

		$client_value    = $client->get_sporting_activities();
		$volunteer_value = $volunteer->get_sporting_activities();
		$this->calculate_property( $client_value, $volunteer_value, 'Sporting Activities', 1 );

		$client_value    = $client->get_watching_sports_list();
		$volunteer_value = $volunteer->get_watching_sports_list();
		$this->calculate_property( $client_value, $volunteer_value, 'Sport Watching Activities', 1 );

		$client_value    = $client->get_movie_genres();
		$volunteer_value = $volunteer->get_movie_genres();
		$this->calculate_property( $client_value, $volunteer_value, 'Movie Watching Activities', 1 );

		$client_value    = $client->get_tv_show_genres();
		$volunteer_value = $volunteer->get_tv_show_genres();		$this->calculate_property( $client_value, $volunteer_value, 'TV Watching Activities', 1 );

		$client_value    = $client->get_other_indoor_activities();
		$volunteer_value = $volunteer->get_other_indoor_activities();		$this->calculate_property( $client_value, $volunteer_value, 'Other Indoor Activities', 1 );

		$client_value    = $client->get_availability();
		$volunteer_value = $volunteer->get_availability();
		$this->calculate_property( $client_value, $volunteer_value, 'Availability', 0 );

		$client_value    = $client->get_time_preferred();
		$volunteer_value = $volunteer->get_time_preferred();
		$this->calculate_property( $client_value, $volunteer_value, 'Time preferred', 0 );

		$this->calculate_age_preferred_property( $client, $volunteer, 'Age Preferred' );
		$this->calculate_age_preferred_property( $volunteer, $client, 'Age Preferred' );

		return $this->match_total;
	}

	/**
	 * Compares the passed property of the client and volunteer and in case there is a match, it updates
	 * the match_total score and the matched_properties value.
	 *
	 * @since    0.0.1
	 * @param    int               $client_value             The value of the client property.
	 * @param    int               $volunteer_value          The value of the volunteer property.
	 * @param    string            $property_key             The name of the property.
	 * @param    int               $type                     Key property that defines whether the property
	 *                                                       can have only one value(0) or multiple values(1).
	 * @return    boolean                                    True of the the property affected the match match_total
	 *                                                       False otherwise.
	 */
	private function calculate_property( $client_value, $volunteer_value, $property_key, $type ) {
		if ( ( ! empty( $client_value ) ) || ! empty( $volunteer_value ) ) {
			$this->matched_properties += 1;
			if ( $client_value === $volunteer_value ) {
				/** A property that can have only one value. */
				if ( $type === 0 ) {
					$my_property        = $this->get_match_constants()->get_value( $property_key );
					$this->match_total += $my_property->get_weight();
				} else { /** A property that can have multiple values. */
					/** How many values are the same? The entry will come as comma separated values, thus we
					 * explode the string into an array and use array_intercept to get the common values.
					 */
					$matched_values     = array_intersect( explode( ',', $client_value ), explode( ',', $volunteer_value ) );
					$my_property        = $this->get_match_constants()->get_value( $property_key );
					$this->match_total += $my_property->get_options_value() * count( $matched_values ) * $my_property->get_weight();
				}
				return true;
			}
		} else {
			return false;
		}
	}
	/**
	 * Finds out if participant_two falls within the age range that participant_one specified.
	 * If participant_one does not have a preference and selected 'No Preference' then it's
	 * assumed that a it's a point and the match_total score is changed.
	 *
	 * @since    0.0.7
	 * @param    Participant          $participant_one           The participant with the desired age range we
	 *                                                           are trying to fulfill.
	 * @param    Participant          $participant_two           The participant we are trying to find out if
	 *                                                           they fulfill the age range from $participant_one
	 */
	private function calculate_age_preferred_property( $participant_one, $participant_two, $property_key ) {
		if ( empty( $participant_one->get_age_preferred() ) ) {
			return;
		}
		/** Participant might have various choices, turn them into an array. */
		$participant_one_desired_age_range = explode( ',', $participant_one->get_age_preferred() );
		/** The age range of $participant_two is only a value. */
		$participant_two_age_range         = $participant_two->get_age_range();
		/** Figure out if participant_one has no preference of if participant_two's age range is in
		 * the array that contains the preferences of $participant_one.
		 */
		if ( in_array( 'No Preference', $participant_one_desired_age_range ) || in_array( $participant_two_age_range, $participant_one_desired_age_range ) ) {
			$this->matched_properties += 1;
			$my_property               = $this->get_match_constants()->get_value( $property_key );
			$this->match_total        += $my_property->get_options_value() * $my_property->get_weight();
		}
	}


	/**
	 * Setter for the match_total property.
	 *
	 * @param string $match_total the new value of the match_total property.
	 */
	public function set_match_total( $match_total ) {
		$this->match_total = $match_total;
		return $this->get_match_total();
	}
	/**
	 * Getter for the match_total property.
	 *
	 * @return string - the value of the match_total property.
	 */
	public function get_match_total() {
		return $this->match_total;
	}

	/**
	 * Setter for the match_constants property.
	 *
	 * @param string $match_constants the new value of the match_constants property.
	 */
	public function set_match_constants( $match_constants ) {
		$this->match_constants = $match_constants;
		return $this->get_match_constants();
	}
	/**
	 * Getter for the match_constants property.
	 *
	 * @return string - the value of the match_constants property.
	 */
	public function get_match_constants() {
		return $this->match_constants;
	}
}
