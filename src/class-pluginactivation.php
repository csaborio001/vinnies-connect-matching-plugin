<?php
/**
 * Logic activates / deactivates the plugin.
 *
 * @package compeer-matching-plugin
 */

namespace ScorpioTek;
/**
 * Exposes Activation/Deactivation procedures for WordPress plugins.
 */
class PluginActivation {

	/**
	 * The hook name to be used when setting up te cron job.
	 *
	 * @var string hook_name
	 */
	private $hook_name;
	/**
	 * How often the task needs to run - hourly, daily, weekly?.
	 *
	 * @var string frequency
	 */
	private $frequency;
	/**
	 * The pathname to the plugin file that invokes this class..
	 *
	 * @var string file_base
	 */
	private $file_base;
	/**
	 * An array that contains the instance and the function name of the instance that will
	 * be invoked when the plugin is activated. The index of the array holds the function
	 * name and the value of the index is the instance of the class. So basically:
	 *
	 * array(
	 * 	'function_name' => $class_instance,
	 * )
	 *
	 * @var array activation_function_list
	 */
	private $activation_function_list;
	/**
	 * An array that contains the instance and the function name of the instance that will
	 * be invoked when the plugin is deactivated. The index of the array holds the function
	 * name and the value of the index is the instance of the class. So basically:
	 *
	 * array(
	 * 	'function_name' => $class_instance,
	 * )
	 *
	 * @var array activation_function_list
	 */
	private $deactivation_function_list;

	public function __construct( $caller_file_name, $hook_name, $frequency ) {
		$this->set_hook_name( $hook_name );
		$this->set_frequency( $frequency );
		$this->set_file_base( plugin_dir_path( dirname( __FILE__ ) ) . $caller_file_name );
	}

	public static function get_instance( $caller_file_name, $hook_name, $frequency ) {
		static $plugin;
		if ( ! isset( $plugin ) ) {
			$plugin = new PluginActivation( $caller_file_name, $hook_name, $frequency );
		}
		return $plugin;
	}

	public function init() {
			\register_activation_hook( $this->get_file_base(), array( $this, 'activate_plugin' ) );
			\register_deactivation_hook( $this->get_file_base(), array( $this, 'deactivate_plugin' ) );
	}

	public function activate_plugin() {
		$hook_name = $this->get_hook_name();
		if ( WP_DEBUG ) {
			error_log( "Hook to activate {$hook_name} plugin invoked, will try to set up the cron event." );
		}
		if ( ! wp_next_scheduled( $hook_name ) ) {
			$result = wp_schedule_event( time(), $this->get_frequency(), $hook_name );
			if ( WP_DEBUG ) {
				if ( true === $result ) {
					error_log( "Cron task with name {$hook_name} successfully scheduled." );
				} else {
					error_log( "Cron task with name {$hook_name} was not scheduled." );
				}
			}
		} else {
			if ( WP_DEBUG ) {
				error_log( "Hook {$hook_name} was already scheduled to run, no need to schedule the event." );
			}
		}

		if ( ! empty( $this->get_activation_function_list() ) && is_array( $this->get_activation_function_list() ) ) {
			foreach ( $this->get_activation_function_list() as $function_name => $class_instance ) {
				if ( method_exists( $class_instance, $function_name ) ) {
					\call_user_func( array( $class_instance, $function_name ) );
				}
			}
		}
	}

	public function deactivate_plugin() {
		wp_clear_scheduled_hook( $this->get_hook_name() );
		if ( ! empty( $this->get_deactivation_function_list() ) && is_array( $this->get_deactivation_function_list() ) ) {
			foreach ( $this->get_deactivation_function_list() as $function_name => $class_instance ) {
				if ( method_exists( $class_instance, $function_name ) ) {
					\call_user_func( array( $class_instance, $function_name ) );
				}
			}
		}
	}

	public function set_cron_callback( $class_instance, $callback_name ) {
		$hook_name = $this->get_hook_name();
		if ( method_exists( $class_instance, $callback_name ) ) {
			add_action( $this->get_hook_name(), array( $class_instance, $callback_name ) );
			if ( WP_DEBUG ) {
				error_log( "Callback function {$callback_name} was successfully set for hook {$hook_name}." );
			}
		} else {
			if ( WP_DEBUG ) {
				error_log( "Callback function {$callback_name} was NOT set for hook {$hook_name}." );
			}
		}
	}

	/**
	 * Setter for the hook_name property.
	 *
	 * @param string $hook_name the new value of the hook_name property.
	 */
	public function set_hook_name( $hook_name ) {
		$this->hook_name = $hook_name;
		return $this->get_hook_name();
	}
	/**
	 * Getter for the hook_name property.
	 *
	 * @return string - the value of the hook_name property.
	 */
	public function get_hook_name() {
		return $this->hook_name;
	}

	/**
	 * Setter for the frequency property.
	 *
	 * @param string $frequency the new value of the frequency property.
	 */
	public function set_frequency( $frequency ) {
		$this->frequency = $frequency;
		return $this->get_frequency();
	}
	/**
	 * Getter for the frequency property.
	 *
	 * @return string - the value of the frequency property.
	 */
	public function get_frequency() {
		return $this->frequency;
	}

	/**
	 * Setter for the file_base property.
	 *
	 * @param string $file_base the new value of the file_base property.
	 */
	public function set_file_base( $file_base ) {
		$this->file_base = $file_base;
		return $this->get_file_base();
	}
	/**
	 * Getter for the file_base property.
	 *
	 * @return string - the value of the file_base property.
	 */
	public function get_file_base() {
		return $this->file_base;
	}


	/**
	 * Setter for the activation_function_list property.
	 *
	 * @param string $activation_function_list the new value of the activation_function_list property.
	 */
	public function set_activation_function_list( $activation_function_list ) {
		$this->activation_function_list = $activation_function_list;
		return $this->get_activation_function_list();
	}
	/**
	 * Getter for the activation_function_list property.
	 *
	 * @return string - the value of the activation_function_list property.
	 */
	public function get_activation_function_list() {
		return $this->activation_function_list;
	}


	/**
	 * Setter for the deactivation_function_list property.
	 *
	 * @param string $deactivation_function_list the new value of the deactivation_function_list property.
	 */
	public function set_deactivation_function_list( $deactivation_function_list ) {
		$this->deactivation_function_list = $deactivation_function_list;
		return $this->get_deactivation_function_list();
	}
	/**
	 * Getter for the deactivation_function_list property.
	 *
	 * @return string - the value of the deactivation_function_list property.
	 */
	public function get_deactivation_function_list() {
		return $this->deactivation_function_list;
	}

}
