<?php
/**
 * Creates the required tables for the Matching plugin.
 *
 * @package compeer-matching-plugin
 */

namespace ScorpioTek;

class TableMaker {
	public function create_matching_tables() {
		global $wpdb;
		global $matching_db_version;

		$table_name = $wpdb->prefix . 'participant_distances';

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
			comparison_id mediumint(9) NOT NULL AUTO_INCREMENT,
			client_id mediumint(9) NOT NULL,
			volunteer_id mediumint(9) NOT NULL,
			distance decimal(9,3) NOT NULL,
			time_car datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			time_public datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			time_walk datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			last_run datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			PRIMARY KEY  (comparison_id, volunteer_id, client_id)
			) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		$table_name = $wpdb->prefix . 'participant_match_score';

		$sql = "CREATE TABLE $table_name (
			match_score_id mediumint(9) NOT NULL AUTO_INCREMENT,
			client_id mediumint(9) NOT NULL,
			volunteer_id mediumint(9) NOT NULL,
			match_score decimal(9,3) NOT NULL,
			last_run datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			PRIMARY KEY  (match_score_id, volunteer_id, client_id)
			) $charset_collate;";

		dbDelta( $sql );

		add_option( 'matching_db_version', $matching_db_version );		
	}

	/**
	 * Deletes the wp_lga table from the WordPress site's DB.
	 *
	 * @since    0.0.1
	 */
	public function delete_matching_tables() {
		global $wpdb;
		$table_name = $wpdb->prefix . 'participant_distances';
		$sql        = "DROP TABLE IF EXISTS $table_name";
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		$wpdb->query( $sql );

		$table_name = $wpdb->prefix . 'participant_match_score';
		$sql        = "DROP TABLE IF EXISTS $table_name";
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		$wpdb->query( $sql );

		delete_option( 'matching_db_version' );
	}
}
