<?php
/**
 * Implements the logging mechanism using MonoLogger
 *
 * @package compeer-matching-plugin
 */

namespace ScorpioTek;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
/**
 * A simple class to be inherited, all classes will have logging mechanism that way.
 */
class FancyLogger {
	/**
	 * The Monolog\Logger class is an open-source library used to log information.
	 *
	 * @var Logger logger - an instance of the Monolog\Logger used to write messages to console.
	 */
	private $logger;
	/**
	 * Simple constructor, initializes the logger if WP_DEBUG flag is raised.
	 */
	public function __construct() {
		/**
		 * Initialize the logger instance.
		 */
		if ( WP_DEBUG && class_exists( Logger::class ) ) {
			$this->set_logger( new Logger( 'scorpiotek-trigger-logic' ) );
			$this->get_logger()->pushHandler( new StreamHandler( WP_CONTENT_DIR . '/debug.log', Logger::DEBUG ) );
			$this->get_logger()->notice( 'Logger initialized and working in Match constants.' );
		}
	}

	/**
	 * Setter for the logger property.
	 *
	 * @param string $logger the new value of the logger property.
	 */
	public function set_logger( $logger ) {
		$this->logger = $logger;
		return $this->get_logger();
	}
	/**
	 * Getter for the logger property.
	 *
	 * @return string - the value of the logger property.
	 */
	public function get_logger() {
		return $this->logger;
	}	
}
