<?php 

namespace ScorpioTek;

use ScorpioTek\Location_Score;
use ScorpioTek\Match_Logic;
use Compeer\UserManagement;
use Compeer\Client;
use Compeer\Volunteer;

global $wpdb;
global $matching_db_version;

class Match_Score extends FancyLogger {
	private $field_to_calculate;

	/**
	 * TODO: Setting $field as an optional parameter as it is not used anywhere else
	 * in this class, will most likely is not needed.
	 */
	public function __construct( $field = null ) {
		parent::__construct();
		if ( ! empty( $field ) ) {
			$this->field_to_calculate = $field;
		}
	}

	public function get_users_role( $role ) {
		$args = array(
			'role'    => $role,
			'orderby' => 'user_nicename',
			'order'   => 'ASC',
		);

		$users = get_users( $args );

		return $users;
	}

	public function save_match_score( $client_user_id, $volunteer_user_id, $score ) {
		global $wpdb;
		$table_name = $wpdb->prefix . 'participant_match_score';
		$total_rows = $wpdb->get_var( $wpdb->prepare( "SELECT count(1) FROM $table_name WHERE client_id = %s and  volunteer_id = %s", $client_user_id, $volunteer_user_id ) );

		if ( $total_rows > 0 ) {
			$wpdb->update(
				$table_name,
				array(
					'client_id'    => $client_user_id,
					'volunteer_id' => $volunteer_user_id,
					'match_score'  => $score,
					'last_run'     => ( new \DateTime( 'now', new \DateTimeZone( 'Australia/Sydney' ) ) )->format( 'Y-m-d h:i:s' ),
				),
				array(
					'client_id'    => $client_user_id,
					'volunteer_id' => $volunteer_user_id,
				)
			);
		} else {
			$wpdb->insert(
				$table_name,
				array(
					/** Replaced non-existing variables $lq_name, and $lq_descrip, with
					 * the ones we set to collect the data - $name and $description
					 **/
					'client_id'    => $client_user_id,
					'volunteer_id' => $volunteer_user_id,
					'match_score'   => $score,
					'last_run'     => ( new \DateTime( 'now', new \DateTimeZone( 'Australia/Sydney' ) ) )->format( 'Y-m-d h:i:s' ),
				)
			);
		}
	}


	public function save_distance_info( $client_user_id, $volunteer_user_id, $score, $time_car, $time_public, $time_walk, $distance ) {
		global $wpdb;
		$table_name = $wpdb->prefix . 'participant_distances';
		/** Try and find to see if the row already exists. */
		$total_rows = $wpdb->get_var(
			$wpdb->prepare(
				"SELECT count(1) FROM $table_name WHERE client_id = %s and  volunteer_id = %s",
				$client_user_id,
				$volunteer_user_id
			)
		);

		if ( $total_rows > 0 ) {
			$wpdb->update(
				$table_name,
				array(
					'client_id'    => $client_user_id,
					'volunteer_id' => $volunteer_user_id,
					'distance'     => $distance,
					'last_run'     => ( new \DateTime( 'now', new \DateTimeZone( 'Australia/Sydney' ) ) )->format( 'Y-m-d h:i:s' ),
					'time_car'     => $time_car,
					'time_public'  => $time_public,
					'time_walk'    => $time_walk,
				),
				array(
					'client_id'    => $client_user_id,
					'volunteer_id' => $volunteer_user_id,
				)
			);
		} else {
			$wpdb->insert(
				$table_name,
				array(
					'client_id'     => $client_user_id,
					'volunteer_id'  => $volunteer_user_id,
					'distance'      => $score,
					'last_run'     => ( new \DateTime( 'now', new \DateTimeZone( 'Australia/Sydney' ) ) )->format( 'Y-m-d h:i:s' ),
					'time_car'      => $time_car,
					'time_public'   => $time_public,
					'time_walk'     => $time_walk,
				)
			);
		}
	}

	public function calculate_all_scores() {
		/** We only need to update/create entries for those participants that are
		 * available for matching.
		 */
		$score                    = 0;
		$client_query_results     = UserManagement::get_available_clients();
		$volunteers_query_results = UserManagement::get_available_volunteers();

		foreach ( $client_query_results->posts as $client_id ) {
			foreach ( $volunteers_query_results->posts as $volunteer_id ) {
				$my_match_logic = new Match_Logic();

				$score = $my_match_logic->calculate_size( $client_id, $volunteer_id );

				/** We're doing strict comparisons with addresses, otherwise we don't take location into account. */
				$client            = new Client( $client_id );
				$volunteer         = new Volunteer( $volunteer_id );
				$can_compute_location_score = UserManagement::can_compute_location_score( $client, $volunteer );

				if ( $can_compute_location_score ) {
					$my_location_score = new Location_Score(
						$client->get_street_1() . ',' . $client->get_suburb() . ',' . $client->get_postcode() . ',AU', 
						$volunteer->get_street_1() . ',' . $volunteer->get_suburb() . ',' . $volunteer->get_postcode() . ',AU',
						$client->get_types_of_transport(),
						$client->get_travel_how_long(),
						$volunteer->get_travel_how_long()
					);
					$score += $my_location_score->get_score();

					$this->save_distance_info(
						$client_id,
						$volunteer_id,
						$my_location_score->distance,
						$my_location_score->time_car,
						$my_location_score->time_public,
						$my_location_score->time_walk,
						$my_location_score->distance
					);
				}

				$this->save_match_score(
					$client_id,
					$volunteer_id,
					$score
				);
			}
		}
		return $score;
	}
}
