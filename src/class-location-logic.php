<?php 

namespace ScorpioTek;

class Location_Logic extends FancyLogger {
	/**
	 * Holds the API key required to query Google's services.
	 *
	 * @var string geokey
	 */
	private $geokey;

	public $distance;

	public function __construct() {
		parent::__construct();
		$this->set_geokey( 'AIzaSyDRvdjD2rl7ga9eZsAsQe2BG4nO0FGr038' );
	}

	/**
	 * Setter for the geokey property.
	 *
	 * @param string $geokey the new value of the geokey property.
	 */
	public function set_geokey( $geokey ) {
		$this->geokey = $geokey;
		return $this->get_geokey();
	}
	/**
	 * Getter for the geokey property.
	 *
	 * @return string - the value of the geokey property.
	 */
	public function get_geokey() {
		return $this->geokey;
	}

	/**
	 * Setter for the logger property.
	 *
	 * @param string $logger the new value of the logger property.
	 */
	public function set_logger( $logger ) {
		$this->logger = $logger;
		return $this->get_logger();
	}

	/**
	 * Getter for the logger property.
	 *
	 * @return string - the value of the logger property.
	 */
	public function get_logger() {
		return $this->logger;
	}

	/**
	 * Figures out the time between two locations based on type of transport.
	 *
	 * @since    0.0.1
	 * @param    string               $origin             The first location address.
	 * @param    string               $destination        The second location address.
	 * @param    string               $transit            The type of transport to use (car, public).
	 * @return   mixed              The entity type associated with this registration page or false if the page could not be found..
	 */
	public function geocode( $origin, $destination, $transit ) {
		// url encode the address
		$origin = urlencode( $origin );

		$destination = urlencode( $destination );

		switch ( $transit ) {
			case 'car':
				$mode = 'driving';
				break;
			case 'public':
				$mode = 'transit';
				$transit = 'bus';
				break;
			case 'taxi':
				$mode = 'driving';
				break;
			case 'walk':
				$mode = 'walking';
				break;
			default:
				$mode = 'driving';
				break;
		}
		
		$geokey = $this->get_geokey();
		$url    = "https://maps.googleapis.com/maps/api/distancematrix/json?&origins={$origin}&destinations={$destination}&mode=$mode&transit_mode={$transit}&key={$geokey}";

		/** Get the json response. */ 
		$distance_response = file_get_contents( $url );

		/** Decode the json. */
		$distance = json_decode( $distance_response, true );

		if ( is_array( $distance ) && 1 <= count( $distance ) &&  $distance['status'] === 'OK' ) {
			$check_origin      = $distance['origin_addresses'][0];
			$check_destination = $distance['destination_addresses'][0];

			if ( empty( $check_origin ) || empty( $check_destination ) ) {
				if ( WP_DEBUG ) {
					$this->get_logger()->error( '<p>Destination or origin address not found</p>' );
				}
				return false;
				// return '<p>Destination or origin address not found</p>';
			}
			$elements        = $distance['rows'][0]['elements'];
			$this->distance  = $elements[0]['distance']['value'];
			$time_in_seconds = $elements[0]['duration']['value'];
			return $time_in_seconds / HOUR_IN_SECONDS;
		} else {
			$this->get_logger()->error( "<strong>ERROR: {$distance['status']}</strong>" );
			return false;
		}
	}
}
