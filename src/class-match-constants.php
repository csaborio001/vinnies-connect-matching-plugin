<?php
/**
 * Contains the definition of the Match_Constants class.
 *
 * @package compeer-matching-plugin
 */

namespace ScorpioTek;

use ScorpioTek\FancyLogger;

/**
 * Class contains the definitions of the property types.
 */
class Match_Constants extends FancyLogger {
	public function __construct() {
		parent::__construct();
	}

	public function get_value( $field ) {
		$result = null;

		switch ( $field ) {
			case 'Living Situation':
				$result = new Property_Size( 1, get_field( 'weight_living_situation', 'option' ) );
				break;
			case 'Marital Status':
				$result = new Property_Size( 1, get_field( 'weight_marital_status', 'option' ) );
				break;
			case 'LGTBI':
				$result = new Property_Size( 1, get_field( 'weight_lgtbi', 'option' ) );
				break;
			case 'ATSI':
				$result = new Property_Size( 1, get_field( 'weight_atsi', 'option' ) );
				break;
			case 'Smoker':
				$result = new Property_Size( 1, get_field( 'weight_smoker', 'option' ) );
				break;
			case 'Country of Birth':
				$result = new Property_Size( 1, get_field( 'weight_country_of_birth', 'option' ) );
				break;
			case 'Languages Spoken':
				$result = new Property_Size( $this->get_number_of_field_options( 36 ), get_field( 'weight_languages_spoken', 'option' ) );
				break;
			case 'Outdoor Activities':
				$result = new Property_Size( $this->get_number_of_field_options( 39 ), get_field( 'weight_outdoor_activities', 'option' ) );
				break;
			case 'Sporting Activities':
				$result = new Property_Size( $this->get_number_of_field_options( 42 ), get_field( 'weight_sporting_activities', 'option' )  );
				break;
			case 'Sport Watching Activities':
				$result = new Property_Size( $this->get_number_of_field_options( 43 ), get_field( 'weight_sport_watching_activities', 'option' )  );
				break;
			case 'Movie Watching Activities':
				$result = new Property_Size( $this->get_number_of_field_options( 48 ), get_field( 'weight_movie_watching_activities', 'option' )  );
				break;
			case 'TV Watching Activities':
				$result = new Property_Size( $this->get_number_of_field_options( 45 ), get_field( 'weight_tv_watching_activities', 'option' )  );
				break;
			case 'Other Indoor Activities':
				$result = new Property_Size( $this->get_number_of_field_options( 46 ), get_field( 'weight_other_indoor_activities', 'option' )  );
				break;
			case 'Availability':
				$result = new Property_Size( 1, get_field( 'weight_availability', 'option' ) );
				break;
			case 'Time preferred':
				$result = new Property_Size( $this->get_number_of_field_options( 58 ), get_field( 'weight_time_preferred', 'option' ) );
				break;
			case 'Age Preferred':
				$result = new Property_Size( 4, get_field( 'weight_age_preferred', 'option' ) );
				break;
		}
		return $result;
	}

	/**
	 * Uses the GFAPI to figure out the number of elements that a multiselect control can have.
	 * If the controls contains an "Other' option, it does not take it into account as the number of choices.
	 * If the control control contains more than 15 elements, then a value of 4 is used, this will most likely
	 * only affect the languages option.
	 *
	 * @since    0.0.7
	 * @param                   int             $field_id      the gravity form field ID of the multiselect control.
	 * @param                   int             $form          the form ID that contains the control.
	 * @return                  int                            the total number of items.
	 */
	private function get_number_of_field_options( $field_id, $form = 7 ) {
		$multi_select_control = \GFFormsModel::get_field( $form, $field_id);
		$number_of_options    = count( $multi_select_control->choices );
		/** There are a lot of languages available, so we'll scale down to 10 to have some sort of
		 * comparing mechanism that is valid, otherwise the number would be incredibly small.
		 */
		if ( $number_of_options > 20 ) {
			return 10;
		}
		/** Find out if the select has an Other option - if it does, then it should not be accounted for. */
		if ( strtolower( $multi_select_control->choices[ $number_of_options -1 ]['text'] ) === 'other' ) {
			return $number_of_options - 1;
		} else {
			return $number_of_options;
		}
	}
}
