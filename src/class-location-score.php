<?php
/**
 * Holds the definition of the Location_Score class.
 *
 * @package compeer-matching-plugin
 */

namespace ScorpioTek;

use ScorpioTek\Location_Logic;
/**
 * Holds the functionality to give a score based on the distance between the client and the volunteer
 * and also taking into consideration how long they are willing to travel.
 */
class Location_Score extends FancyLogger {

	const MINUTES_IN_HOURS       = 60;
	const MAX_LOCATION_SCORE     = 2;
	const TRAVEL_SCORE_THRESHOLD = 41;
	/**
	 * This variable is not used, is it necessary>.
	 *
	 * @var ??? options_value
	 */
	private $options_value;
	/**
	 * This variable is not used, is it necessary?
	 *
	 * @var ??? weight
	 */
	private $weight;
	/**
	 * The starting point of the trip, as an address.
	 *
	 * @var string origin
	 */
	private $origin;
	/**
	 * The end point of the trip, as an address.
	 *
	 * @var string destination
	 */
	private $destination;
	/**
	 * The type of transport to be used.
	 *
	 * @var string transportation
	 */
	private $transportation;
	/**
	 * How long the client is willing to travel.
	 *
	 * @var int client_how_long
	 */
	private $client_how_long;
	/**
	 * How long the volunteer is willing to travel.
	 *
	 * @var string volunteer_how_long
	 */
	private $volunteer_how_long;

	public $time_car    = 0;
	public $time_public = 0;
	public $time_taxi   = 0;
	public $time_walk   = 0;
	public $distance 	= 0;

	/**
	 *Initializes the values of the properties of the Location_Score class.
	 *
	 * @since    0.0.1
	 * @param    string               $origin                The starting point of the trip, as an address.
	 * @param    string               $destination           The end point of the trip, as an address.
	 * @param    string               $transportation        The type of transport to be used.
	 * @param    string               $client_how_long       How long the client is willing to travel.
	 * @param    string               $volunteer_how_long     How long the volunteer is willing to travel.
	 */
	public function __construct( $origin, $destination, $transportation, $client_how_long, $volunteer_how_long ) {
		parent::__construct();
		$this->origin               = $origin;
		$this->destination          = $destination;
		$this->transportation       = $transportation;
		$this->client_how_long      = $client_how_long;
		$this->volunteer_how_long   = $volunteer_how_long;
	}

	/**
	 * Figures out the score based on the properties defined for the instance of the class.
	 *
	 * @since    0.0.1
	 * @return   int                 The time it gets (in seconds) to get from origin to destination.
	 */
	public function get_score() {
		$my_location_logic = new Location_Logic();

		$types_of_transport = explode( ',', $this->transportation );

		$shortest_time = 10000000;

		foreach ( $types_of_transport as $type_of_transport ) {
			switch ( $type_of_transport ) {
				case 'Car':
					$this->time_car = $my_location_logic->geocode( $this->origin, $this->destination, 'car' );

					$this->distance = $my_location_logic->distance;
					break;
				case 'Public Transport':
					$this->time_public = $my_location_logic->geocode( $this->origin, $this->destination, 'public' );

					$this->distance = $my_location_logic->distance;
					break;
				case 'Taxi':
					$this->time_taxi = $my_location_logic->geocode( $this->origin, $this->destination, 'taxi' );

					$this->distance = $my_location_logic->distance;
					break;
				case 'Walk':
					$this->time_walk = $my_location_logic->geocode( $this->origin, $this->destination, 'walk' );

					$this->distance = $my_location_logic->distance;
					break;
			}
		}

		if ( ( $this->time_car > 0 ) && ( $this->time_car < $shortest_time ) ) {
			$shortest_time = $this->time_car;
		}

		if ( ( $this->time_public > 0 ) && ( $this->time_public < $shortest_time ) ) {
			$shortest_time = $this->time_public;
		}

		if ( ( $this->time_taxi > 0 ) && ( $this->time_taxi < $shortest_time ) ) {
			$shortest_time = $this->time_taxi;
		}

		if ( ( $this->time_walk > 0 ) && ( $this->time_walk < $shortest_time ) ) {
			$shortest_time = $this->time_walk;
		}

		$client_total    = $this->get_total( $this->client_how_long, $shortest_time * self::MINUTES_IN_HOURS );
		$volunteer_total = $this->get_total( $this->volunteer_how_long, $shortest_time * self::MINUTES_IN_HOURS);
		return $client_total + $volunteer_total;
	}

	public function get_total( $desired_travel_time_in_minutes, $total_travel_time_in_minutes ) {
		if ( $total_travel_time_in_minutes <= $desired_travel_time_in_minutes ) {
			return self::MAX_LOCATION_SCORE;
		}
		/** Total Travel Time is more than the desired travel time, figure out the difference. */
		$time_difference = $total_travel_time_in_minutes - $desired_travel_time_in_minutes;
		/** The difference is quite substantial, no points awaraded! */
		if ( $time_difference >= self::TRAVEL_SCORE_THRESHOLD ) {
			return 0;
		} else { /** Figure out the distance using the formula MAX_LOCATION_SCORE – ( 0.05 * TIME_DIFFERENCE ) */
			return self::MAX_LOCATION_SCORE - ( 0.05 * $time_difference );
		}
		return 0;
	}
}
