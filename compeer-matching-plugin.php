<?php
/**
 * Plugin Name: Matching plugin
 * Plugin URI: http://www.compeer.com/compeer-matching-plugin
 * Description: Plug in to do the calculation of the compeer site matching
 * Version: 1.0.2
 * Author: Scorpiotek
 * Author URI: http://www.scorpiotek.com
 */

require_once 'vendor/autoload.php';

use ScorpioTek\PluginActivation;
use ScorpioTek\Match_Score;
use ScorpioTek\TableMaker;

$match_score                = new Match_Score();
$table_maker                = new TableMaker();
$plugin_activation_instance = PluginActivation::get_instance( 'compeer-matching-plugin.php', 'vinnies_connect_matching_cron', 'daily' );
$plugin_activation_instance->set_activation_function_list( array( 'create_matching_tables' => $table_maker ) );
$plugin_activation_instance->set_deactivation_function_list( array( 'delete_matching_tables' => $table_maker ) );
$plugin_activation_instance->set_cron_callback( $match_score, 'calculate_all_scores' );
$plugin_activation_instance->init();




