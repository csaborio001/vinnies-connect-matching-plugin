# Compeer Matching Plugin

This plugin will run the required tasks to figure out the compatibility score between all volunteers and clients in the Compeer system.

## Version History

### 1.0.2

* Plugin implenents new algorithm to figure match scores ( 2 - 0.005 * Distance Delta )
* Tests added to test a variety of situations and locations.
 
### 1.0.1

* Fixed syntax in MatchScore, refactored and cleaned code a little bit
* Plugin Activation in Place
* Changed the logic on plugin activation and deactivation, everything is now done with methods from class-pluginactivation.php. Removed all the old references that, tables are now created and deleted
on plugin activation and deactivation respectively.

### 1.0

* Initial Version
* Tests Added