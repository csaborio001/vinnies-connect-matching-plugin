<?php

use ScorpioTek\Location_Score;
use ScorpioTek\WordPressLibBootStrap;


class LocationScoreTest extends \Codeception\TestCase\WPTestCase {
		/**
	 * @var \WpunitTester
	 */
	protected $tester;
	protected $my_Location_Score;

	public function setUp(): void {
		// Before...
		parent::setUp();
	}

	public function tearDown(): void {
		// Your tear down methods here.
		// Then...
		parent::tearDown();
	}

	/**
	 * Setter for the wordpress_lib_bootstrap property.
	 *
	 * @param string $wordpress_lib_bootstrap the new value of the wordpress_lib_bootstrap property.
	 */
	public function set_wordpress_lib_bootstrap( $wordpress_lib_bootstrap ) {
		$this->wordpress_lib_bootstrap = $wordpress_lib_bootstrap;
		return $this->get_wordpress_lib_bootstrap();
	}
	/**
	 * Getter for the wordpress_lib_bootstrap property.
	 *
	 * @return string - the value of the wordpress_lib_bootstrap property.
	 */
	public function get_wordpress_lib_bootstrap() {
		return $this->wordpress_lib_bootstrap;
	}
}
