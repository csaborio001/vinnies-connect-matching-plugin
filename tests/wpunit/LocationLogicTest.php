<?php
/**
 * Contains the definition of the Location_Logic class.
 *
 * @package compeer-matching-plugin
 */

use ScorpioTek\Location_Logic;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use ScorpioTek\WordPressLibBootStrap;
/**
 * Class to test the Location_Logic class.
 */
class LocationLogicTest extends \Codeception\TestCase\WPTestCase {
	/**
	 * The instance of the Location_Logic used to figure out various
	 * location operations..
	 *
	 * @var Location_Logic test_location_logic_instance
	 */
	protected $test_location_logic_instance;

	public function setUp(): void {
		parent::setUp();
		$this->test_location_logic_instance = new Location_Logic();
		$this->set_wordpress_lib_bootstrap( new WordPressLibBootStrap() );
	}

	public function tearDown(): void {
		parent::tearDown();
	}
	/**
	 * Setter for the wordpress_lib_bootstrap property.
	 *
	 * @param string $wordpress_lib_bootstrap the new value of the wordpress_lib_bootstrap property.
	 */
	public function set_wordpress_lib_bootstrap( $wordpress_lib_bootstrap ) {
		$this->wordpress_lib_bootstrap = $wordpress_lib_bootstrap;
		return $this->get_wordpress_lib_bootstrap();
	}
	/**
	 * Getter for the wordpress_lib_bootstrap property.
	 *
	 * @return string - the value of the wordpress_lib_bootstrap property.
	 */
	public function get_wordpress_lib_bootstrap() {
		return $this->wordpress_lib_bootstrap;
	}

	/**
	 * Tests the public transport time between two locations, value should be less than the expected
	 * The time is variant, so assertLessThanOrEqual is used.
	 */
	public function test_public_transportation() {
		$expected = 62; /** Minutes */
		$result   = $this->test_location_logic_instance->geocode(
			'New South Wales 2774,Blaxland,Penrith,Australia',
			'Penrith NSW 2750,Australia',
			'bus'
		);
		$this->assertNotFalse( $result );
		$this->assertLessThanOrEqual( $expected, $result );
	}

	/**
	 * Tests the driving time between two locations, value should be less than the expected
	 * The time is variant, so assertLessThanOrEqual is used.
	 */
	public function test_public_driving() {
		$expected = 34;
		$result = $this->test_location_logic_instance->geocode(
			'New South Wales 2774,Blaxland,Penrith,Australia',
			'Penrith NSW 2750,Australia',
			'car'
		);
		$this->assertNotFalse( $result );
		$this->assertLessThanOrEqual( $expected, $result );
	}

	/**
	 * Tests the time it takes to reach a destination that is the same as the
	 * departure point when going by car and using public transport,
	 */
	public function test_time_between_two_points_by_car_when_start_and_end_are_the_same() {
		$start_address = 'New South Wales 2774,Blaxland,Penrith,Australia';
		$end_address   = 'New South Wales 2774,Blaxland,Penrith,Australia';
		$expected      = 0;
		/** Car */
		$result = $this->test_location_logic_instance->geocode(
			$start_address,
			$end_address,
			'car'
		);
		$this->assertNotFalse( $result );
		$this->assertLessThanOrEqual( $expected, $result );
		/** Public Transport. */
		$result = $this->test_location_logic_instance->geocode(
			$start_address,
			$end_address,
			'bus'
		);
		$this->assertNotFalse( $result );
		$this->assertLessThanOrEqual( $expected, $result );
	}
}


