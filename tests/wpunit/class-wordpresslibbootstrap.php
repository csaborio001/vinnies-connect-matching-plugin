<?php

namespace ScorpioTek;

use Compeer\UserManagement;
use Compeer\Client;
use Compeer\Volunteer;

\Codeception\Util\Autoload::addNamespace('ScorpioTek', 'src');

class WordPressLibBootStrap extends \Codeception\TestCase\WPTestCase {

	/**
	 * @var \WpunitTester
	 */
	protected $tester;

	public function setUp(): void
	{
		// Before...
		parent::setUp();

		// Your set up methods here.
	}

	public function tearDown(): void
	{
		// Your tear down methods here.

		// Then...
		parent::tearDown();
	}

	public function create_user( $participant_type, $participant_email, $first_name = '', $last_name = '' ) {
		if ( empty( $first_name ) ) {
			$first_name = ucwords( $participant_type );
		}
		if ( empty( $last_name ) ) {
			$last_name = $participant_email;
		}
		$participant_user_id = $this->factory->user->create(
			array(
				'user_login' => $participant_email,
				'user_email' => $participant_email,
				'role'       => $participant_type,
				'first_name' => $first_name,
				'last_name'  => $last_name,
			)
		);
		return $participant_user_id;
	}

	public function create_entity_post( $entity_type, $state = 'under_review' ) {
		return $this->factory()->post->create(
			array(
				'post_type' => $entity_type,
				'post_title' => ucwords( $entity_type ) . ' ' . $state,
				'meta_input'  => array(
					'_state' => $state,
				),
			)
		);
	}

	public function create_user_n( $participant_type, $email_domain = 'compeer.com', $num_users = 1 ) {
		$created_user_array = array();

		for ( $i = 0; $i < $num_users; $i++ ) {
		$participant_email = "{$participant_type}{$i}@{$email_domain}";
		$participant_user_id = $this->factory->user->create(
			array(
				'user_login' => $participant_email,
				'user_email' => $participant_email,
				'role'       => $participant_type,
				'first_name' => ucwords( $participant_type ),
				'last_name'  => $i,
			)
		);
		$created_user_array[] = array(
			'user_id' => $participant_user_id,
			'email'   => $participant_email,
		);
	}
	return $created_user_array;
	}

	public function create_participant_instance( $participant_type, $email_domain = 'compeer.com', $property_list = array() ) {
		static $email_address_counter = 0;
		$participant_id               = UserManagement::get_post_id_from_user(
			$this->create_user(
				$participant_type,
				"{$participant_type}{$email_address_counter}@{$email_domain}"
			)
		);
		$participant_instance = null;
		switch ( $participant_type ) {
			case 'client':
				$participant_instance = new Client( $participant_id );
				break;
			case 'volunteer':
				$participant_instance = new Volunteer( $participant_id );
		}

		if ( is_array( $property_list) && ! empty( $property_list ) ) {
			$this->modify_participant_fields( $participant_instance, $property_list );
		}
		++$email_address_counter;
		return $participant_instance;
	}

	function modify_participant_fields( $participant_instance, $property_list ) {
		foreach ( $property_list as $prop_name => $prop_value ) {
			call_user_func( array( $participant_instance, "set_{$prop_name}" ), $prop_value );
		}
	}

	public function create_match( $client_post_id, $volunteer_post_id ) {
		return $this->factory()->post->create(
			array(
				'post_type' => 'match',
				'post_title' => get_the_title( $client_post_id ) . ' and ' . get_the_title( $volunteer_post_id ),
				'meta_input'  => array(
					'_state'                       => 'friendship_in_progress',
					'client_match_object_field'    => $client_post_id,
					'volunteer_match_object_field' => $volunteer_post_id,
				),
			)
		);
	}

	public function get_column_from_table( $table_name, $column_name = '*', $client_id, $volunteer_id ) {
		global $wpdb;
		$result = $wpdb->get_results(
			$wpdb->prepare(
				"
				SELECT {$column_name}
				FROM {$wpdb->prefix}{$table_name}
				WHERE `client_id` = %d 
				AND `volunteer_id` = %d
				",
				$client_id,
				$volunteer_id
			),
			OBJECT
		);
		return $result;
	}
}
