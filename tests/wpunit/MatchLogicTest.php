<?php
/**
 * Contains the definition of the Match_Logic class.
 *
 * @package compeer-matching-plugin
 */

use ScorpioTek\Match_Logic;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use ScorpioTek\WordPressLibBootStrap;
use ScorpioTek\Match_Constants;
use Compeer\Client;
use Compeer\Volunteer;
use Compeer\UserManagement;

require_once get_stylesheet_directory() . '/vendor/autoload.php';

/**
 * Class to test the Location_Logic class.
 */
class MatchLogicTest extends \Codeception\TestCase\WPTestCase {
	/**
	 * The instance of the Location_Logic used to figure out various
	 * location operations..
	 *
	 * @var Location_Logic test_location_logic_instance
	 */
	protected $test_location_logic_instance;

	public function setUp(): void {
		parent::setUp();
		$this->set_wordpress_lib_bootstrap( new WordPressLibBootStrap() );
		$mc = new Match_Constants();
	}

	public function tearDown(): void {
		parent::tearDown();
	}
	/**
	 * Setter for the wordpress_lib_bootstrap property.
	 *
	 * @param string $wordpress_lib_bootstrap the new value of the wordpress_lib_bootstrap property.
	 */
	public function set_wordpress_lib_bootstrap( $wordpress_lib_bootstrap ) {
		$this->wordpress_lib_bootstrap = $wordpress_lib_bootstrap;
		return $this->get_wordpress_lib_bootstrap();
	}
	/**
	 * Getter for the wordpress_lib_bootstrap property.
	 *
	 * @return string - the value of the wordpress_lib_bootstrap property.
	 */
	public function get_wordpress_lib_bootstrap() {
		return $this->wordpress_lib_bootstrap;
	}

	/** Nothing has been set, the result should yield zero. */
	public function test_calculate_property_when_no_properties_are_set() {
		$expected = 0;
		$client_info    = $this->get_wordpress_lib_bootstrap()->create_user_n( 'client' );
		$client_id      = $client_info[0]['user_id'];
		$volunteer_info = $this->get_wordpress_lib_bootstrap()->create_user_n( 'volunteer' );
		$volunteer_id   = $volunteer_info[0]['user_id'];
		$ml             = new Match_Logic();
		$ml->calculate_size( $client_id, $volunteer_id );
		$result = $ml->get_match_total();
		$this->assertEquals( $expected, $result );
	}

	/**
	 * Tests to see of the value of living situation is the correct one when a volunteer and
	 * a client *only* have this value in common.
	 */
	public function test_calculate_property_when_living_situation_match() {
		$expected           = get_field( 'weight_living_situation', 'option' );
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_living_situation( 'Living Alone' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_living_situation( 'Living Alone' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( $expected, $result );
	}

	/**
	 * Tests to see of the value of marital_status is the correct one when a volunteer and
	 * a client *only* have this value in common.
	 */
	public function test_calculate_property_when_marital_status_match() {
		$expected           = get_field( 'weight_marital_status', 'option' );
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_marital_status( 'Single' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_marital_status( 'Single' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( $expected, $result );
	}

	/**
	 * Tests to see of the value of atsi is the correct one when a volunteer and
	 * a client *only* have this value in common.
	 */
	public function test_calculate_property_when_atsi_match() {
		$expected           = get_field( 'weight_atsi', 'option' );
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_atsi( 'Yes' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_atsi( 'Yes' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( $expected, $result );
	}

	/**
	 * Tests to see of the value of smoker is the correct one when a volunteer and
	 * a client *only* have this value in common.
	 */
	public function test_calculate_property_when_smoker_match() {
		$expected           = get_field( 'weight_smoker', 'option' );
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_smoker( 'Yes' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_smoker( 'Yes' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( $expected, $result );
	}

	/**
	 * Tests to see of the value of country_of_birth is the correct one when a volunteer and
	 * a client *only* have this value in common.
	 */
	public function test_calculate_property_when_country_of_birth_match() {
		$expected           = get_field( 'weight_country_of_birth', 'option' );
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_country_of_birth( 'Australia' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_country_of_birth( 'Australia' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( $expected, $result );
	}

	/**
	 * Tests to see if the value of languages_spoken is the correct one when a volunteer and
	 * a client *only* have two properties in common.
	 */
	public function test_calculate_property_when_languages_spoken_match() {
		$expected           = 1 / 10 * 4 * 2; // 0.80
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_languages_spoken( 'Spanish,English' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_languages_spoken( 'Spanish,English' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( $expected, sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of outdoor_activities is the correct one when a volunteer and
	 * a client *only* have two properties in common.
	 */
	public function test_calculate_property_when_outdoor_activities_match() {
		$expected           = 1 / 5 * 3 * 3; // 1.80
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_outdoor_activities( 'Walking,Gardening,Going to the Beach' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_outdoor_activities( 'Walking,Gardening,Going to the Beach' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( $expected, sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of sporting_activities is the correct one when a volunteer and
	 * a client *only* have two properties in common.
	 */
	public function test_calculate_property_when_sporting_activities_match() {
		$expected           = 1 / 9 * 3 * 4;
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_sporting_activities( 'Gym,Running,Swimming,Football' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_sporting_activities( 'Gym,Running,Swimming,Football' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of watching_sports_list is the correct one when a volunteer and
	 * a client *only* have two properties in common.
	 */
	public function test_calculate_property_when_watching_sports_list_match() {
		$expected           = 1 / 10 * 3 * 6;
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_watching_sports_list( 'Swimming,Football,Netball,Basketball,Golf,Tennis' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_watching_sports_list( 'Swimming,Football,Netball,Basketball,Golf,Tennis' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of movie_genres is the correct one when a volunteer and
	 * a client *only* have two properties in common.
	 */
	public function test_calculate_property_when_movie_genres_match() {
		$expected           = 1 / 4 * 3 * 2;
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_movie_genres( 'Comedy,Drama' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_movie_genres( 'Comedy,Drama' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of tv_show_genres is the correct one when a volunteer and
	 * a client *only* have two properties in common.
	 */
	public function test_calculate_property_when_tv_show_genres_match() {
		$expected           = 1 / 7 * 3 * 1;
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_tv_show_genres( 'Documentary' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_tv_show_genres( 'Documentary' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of other_indoor_activities is the correct one when a volunteer and
	 * a client *only* have two properties in common.
	 */
	public function test_calculate_property_when_other_indoor_activities_match() {
		$expected           = 1 / 8 * 3 * 2;
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_other_indoor_activities( 'Cooking,Computers/Social Media' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_other_indoor_activities( 'Cooking,Computers/Social Media' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of availability is the correct one when a volunteer and
	 * a client *only* have two properties in common.
	 */
	public function test_calculate_property_when_availability_match() {
		$expected           = get_field( 'weight_availability', 'option' );
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_availability( 'Weekdays during the day' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_availability( 'Weekdays during the day' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of time_preferred is the correct one when a volunteer and
	 * a client *only* have two properties in common.
	 */
	public function test_calculate_property_when_time_preferred_match() {
		$expected           = 1 / 3 * 3 * 3;
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_time_preferred( 'Morning (before Mid-day),Afternoon (12 PM- 5 PM),Evening (5 PM onwards)' );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_time_preferred( 'Morning (before Mid-day),Afternoon (12 PM- 5 PM),Evening (5 PM onwards)' );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of age_preferred is the correct one when a volunteer and
	 * a client both fall into each other's age preference choice.
	 */
	public function test_calculate_property_when_age_preferred_match_on_both_counts() {
		$expected           = ( 1 / 4 * 5 ) + ( 1 / 4 * 5 ); // 2.50
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_age_preferred( '26-35' );
		/** Set the birth date of client to match the age range the volunteer is looking for. */
		$birth_date_of_forty_year_old = ( new \DateTime('now', new \DateTimeZone('Australia/Sydney') ) )->modify( '-40 years' )->format( 'Y-m-d' );
		$client_instance->set_date_of_birth( $birth_date_of_forty_year_old );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_age_preferred( '36-45' );
		/** Set the birth date of volunteer to match the age range the client is looking for. */
		$birth_date_of_thirty_year_old = ( new \DateTime('now', new \DateTimeZone('Australia/Sydney') ) )->modify( '-30 years' )->format( 'Y-m-d' );
		$volunteer_instance->set_date_of_birth( $birth_date_of_thirty_year_old );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of age_preferred is the correct one when only the volunteer
	 * falls within the range of the client's choices.
	 */
	public function test_calculate_property_when_age_preferred_match_only_on_client() {
		$expected           = ( 1 / 4 * 5 ) + 0 ; // 1.25
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_age_preferred( '26-35' );
		/** Set the birth date of client to match the age range the volunteer is looking for. */
		$birth_date_of_forty_year_old = ( new \DateTime('now', new \DateTimeZone('Australia/Sydney') ) )->modify( '-26 years' )->format( 'Y-m-d' );
		$client_instance->set_date_of_birth( $birth_date_of_forty_year_old );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_age_preferred( '18-25' );
		/** Set the birth date of volunteer to match the age range the client is looking for. */
		$birth_date_of_thirty_year_old = ( new \DateTime('now', new \DateTimeZone('Australia/Sydney') ) )->modify( '-30 years' )->format( 'Y-m-d' );
		$volunteer_instance->set_date_of_birth( $birth_date_of_thirty_year_old );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests to see if the value of age_preferred when client did not specify a choice and the
	 * volunteer did not get his age rage fulfillment.
	 */
	public function test_calculate_property_when_age_preferred_match_only_on_client_no_preference() {
		$expected           = ( 1 / 4 * 5 ) + 0 ; // 1.25
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_age_preferred( 'No Preference' );
		/** Set the birth date of client to match the age range the volunteer is looking for. */
		$birth_date_of_forty_year_old = ( new \DateTime('now', new \DateTimeZone('Australia/Sydney') ) )->modify( '-26 years' )->format( 'Y-m-d' );
		$client_instance->set_date_of_birth( $birth_date_of_forty_year_old );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_age_preferred( '18-25' );
		/** Set the birth date of volunteer to match the age range the client is looking for. */
		$birth_date_of_thirty_year_old = ( new \DateTime('now', new \DateTimeZone('Australia/Sydney') ) )->modify( '-80 years' )->format( 'Y-m-d' );
		$volunteer_instance->set_date_of_birth( $birth_date_of_thirty_year_old );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}

	/**
	 * Tests the match score of a volunteer and a client when they both specified 'No Preference' for the
	 * age_range preference.
	 */
	public function test_calculate_property_when_age_preferred_match_both_no_preference() {
		$expected           = ( 1 / 4 * 5 ) + ( 1 / 4 * 5 ); // 2.50
		$client_instance    = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'client' );
		$result             = $client_instance->set_age_preferred( 'No Preference' );
		/** Set the birth date of client to match the age range the volunteer is looking for. */
		$birth_date_of_forty_year_old = ( new \DateTime('now', new \DateTimeZone('Australia/Sydney') ) )->modify( '-26 years' )->format( 'Y-m-d' );
		$client_instance->set_date_of_birth( $birth_date_of_forty_year_old );
		$volunteer_instance = $this->get_wordpress_lib_bootstrap()->create_participant_instance( 'volunteer' );
		$result             = $volunteer_instance->set_age_preferred( 'No Preference' );
		/** Set the birth date of volunteer to match the age range the client is looking for. */
		$birth_date_of_thirty_year_old = ( new \DateTime('now', new \DateTimeZone('Australia/Sydney') ) )->modify( '-80 years' )->format( 'Y-m-d' );
		$volunteer_instance->set_date_of_birth( $birth_date_of_thirty_year_old );
		$result             = $this->get_the_match_total( $client_instance, $volunteer_instance );
		$this->assertEquals( sprintf( '%.2f', $expected ), sprintf( '%.2f', $result ) );
	}


	public function get_the_match_total( $client_instance, $volunteer_instance ) {
		$ml = new Match_Logic();
		$ml->calculate_size( $client_instance->get_id(), $volunteer_instance->get_id() );
		return $ml->get_match_total();
	}
}
